# RNStarter
![Beta](https://github.com/saurshaz/rn-starter/workflows/Beta/badge.svg)
![Demo](./demo.png)

<p align="center">
  
  <img alt="GitHub release (latest by date)" src="https://img.shields.io/github/v/release/saurshaz/rn-starter">

  <img alt="GitHub" src="https://img.shields.io/github/license/saurshaz/rn-starter">
</p>

---
### A mobile starter template with :: 
- `react-native` with `expo`
- `expo-web` based web
- `react-navigation`
- `nextjs` like structure
- TODO: capability to have `api` code 


- Setup your project with Expo
  - Install the CLI: `npm i -g expo-cli`
  - `cd` into the project `npm i` or `yarn`
  - Start the project with `yarn web`
  - Go to `http://localhost:19006/` to see your project!

### 🏁 New Commands

- **Starting web**
  - ✅ `yarn web`

- **Building web**
  - ✅ `expo build:web` and then serve from any server
